Q�@^<?php exit; ?>a:1:{s:7:"content";s:101116:"a:4:{s:5:"child";a:1:{s:0:"";a:1:{s:3:"rss";a:1:{i:0;a:6:{s:4:"data";s:3:"


";s:7:"attribs";a:1:{s:0:"";a:1:{s:7:"version";s:3:"2.0";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:1:{s:7:"channel";a:1:{i:0;a:6:{s:4:"data";s:49:"
	
	
	
	
	
	
	
	
	
	
		
		
		
		
		
		
		
		
		
	";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:4:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:27:"News –  – WordPress.org";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:26:"https://wordpress.org/news";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:14:"WordPress News";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:13:"lastBuildDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 03 Feb 2020 09:54:06 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"language";a:1:{i:0;a:5:{s:4:"data";s:5:"en-US";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:9:"generator";a:1:{i:0;a:5:{s:4:"data";s:40:"https://wordpress.org/?v=5.4-alpha-47215";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"item";a:10:{i:0;a:6:{s:4:"data";s:57:"
		
		
		
		
		
				
		

					
										
					
		
		
			";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:4:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:36:"The Month in WordPress: January 2020";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:71:"https://wordpress.org/news/2020/02/the-month-in-wordpress-january-2020/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 03 Feb 2020 09:54:06 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:18:"Month in WordPress";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://wordpress.org/news/?p=8316";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:338:"Following an action-packed December, 2020 is off to a fine start with some new releases and announcements. Read on to find out what happened in the WordPress project in January. Release of Gutenberg 7.2 &#38; 7.3 Gutenberg 7.2, the first Gutenberg release of 2020, was deployed on January 8th and included over 180 pull requests [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:10:"Angela Jin";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:5943:"
<p>Following an action-packed December, 2020 is off to a fine start with some new releases and announcements. Read on to find out what happened in the WordPress project in January.</p>



<hr class="wp-block-separator" />



<h2>Release of Gutenberg 7.2 &amp; 7.3</h2>



<p><a href="https://make.wordpress.org/core/2020/01/09/whats-new-in-gutenberg-8-january/">Gutenberg 7.2</a>, the first Gutenberg release of 2020, was deployed on January 8th and included over 180 pull requests from more than 56 contributors. This was followed soon after by <a href="https://make.wordpress.org/core/2020/01/22/whats-new-in-gutenberg-22-january/">Gutenberg 7.3</a>. New features include a new Buttons block, support in adding links to Media &amp; Text block images, improvements to the Navigation and Gallery blocks, performance improvements, and accessibility enhancements. These releases also included many additional enhancements, fixes, new APIs, documentation, and more.</p>



<p>Want to get involved in building Gutenberg? Follow <a href="https://make.wordpress.org/core/">the Core team blog</a>, contribute to <a href="https://github.com/WordPress/gutenberg/">Gutenberg on GitHub</a>, and join the #core-editor channel in <a href="https://make.wordpress.org/chat/">the Making WordPress Slack group</a>.</p>



<h2>Proposal for an XML Sitemaps Feature Plugin</h2>



<p>In June last year, a team of contributors <a href="https://make.wordpress.org/core/2019/06/12/xml-sitemaps-feature-project-proposal/">proposed</a> a feature plugin that would bring standardized XML sitemaps to WordPress Core. Since then, the team has been working to bring this to reality and <a href="https://make.wordpress.org/core/2020/01/27/feature-plugin-xml-sitemaps/">have now published a working plugin</a> to demonstrate this new capability.</p>



<p>The plugin is still in development, but the included features already provide much-needed functionality from which all WordPress sites can benefit. You can install the plugin from your WordPress dashboard or <a href="https://wordpress.org/plugins/core-sitemaps/">download it here</a>.</p>



<p>Want to get involved in bringing this feature to Core? Follow <a href="https://make.wordpress.org/core/">the Core team blog</a>, report any issues you find <a href="https://github.com/GoogleChromeLabs/wp-sitemaps/issues">on GitHub</a>, and join the #core channel in <a href="https://make.wordpress.org/chat/">the Making WordPress Slack group</a>.</p>



<h2>A New Block-Based Themes Meeting</h2>



<p>The Theme Review Team has <a href="https://make.wordpress.org/themes/2020/01/29/new-bi-weekly-block-based-themes-meeting/">announced</a> that they will be holding bi-weekly meetings in the #themereview channel focused on discussing block-based themes. If you are interested in discussing themes within the context of Gutenberg’s full-site editing framework, this will be the place to do so! The first meeting will be held on Wednesday, February 5, at 16:00 UTC.</p>



<p>Want to get involved with the Theme Review Team or <a href="https://make.wordpress.org/themes/handbook/get-involved/become-a-reviewer/">become a reviewer</a>? Follow <a href="https://make.wordpress.org/themes/">their blog</a>, and join the #themereview channel in <a href="https://make.wordpress.org/chat/">the Making WordPress Slack group</a>.</p>



<hr class="wp-block-separator" />



<h2>Further Reading</h2>



<ul><li>The Core team has started work on WordPress 5.4 and kicked off their planning with <a href="https://make.wordpress.org/core/2020/01/14/wordpress-5-4-planning-roundup/">a summary post</a>. You can follow all the v5.4 updates by watching <a href="https://make.wordpress.org/core/tag/5-4/">the version tag</a> on the Core team blog.</li><li>The inaugural <a href="https://2020.asia.wordcamp.org/">WordCamp Asia</a> event is taking place in February. This will be the largest WordPress event in the region, bringing together around 1,500 WordPress enthusiasts from around the world.</li><li>Two WordPress community leaders, <a href='https://profiles.wordpress.org/chanthaboune/' class='mention'><span class='mentions-prefix'>@</span>chanthaboune</a> and <a href='https://profiles.wordpress.org/andreamiddleton/' class='mention'><span class='mentions-prefix'>@</span>andreamiddleton</a>, <a href="https://wordpress.org/news/2020/01/wordpress-leaders-nominated-for-cmx-awards/">were nominated for CMX awards</a> due to their work on the WordPress project, with <a href='https://profiles.wordpress.org/andreamiddleton/' class='mention'><span class='mentions-prefix'>@</span>andreamiddleton</a> winning the award for Executive Leader of a Community Team.</li><li><a href="https://make.wordpress.org/core/2020/01/29/lazy-loading-images-in-wordpress-core/">A feature plugin has been proposed</a> that introduces lazy-loading images to WordPress Core, which will be a huge step forward in improving performance all across the web.</li><li>The Core team has put together <a href="https://make.wordpress.org/core/handbook/tutorials/faq-for-new-contributors/">an extensive and informative FAQ</a> to help new contributors get involved in contributing to the project.</li><li>One key priority for Gutenberg is the ability to control the block editor. There are already a number of APIs that control the experience, but there is a lack of consistency and missing APIs. <a href="https://make.wordpress.org/core/2020/01/23/controlling-the-block-editor/">A method to address this</a> has been proposed.</li><li>The Design team <a href="https://make.wordpress.org/design/2020/01/24/gutenberg-phase-2-friday-design-update-43/">published detailed information</a> on the recent design improvements in Gutenberg.</li></ul>



<p><em>Have a story that we should include in the next “Month in WordPress” post? Please </em><a href="https://make.wordpress.org/community/month-in-wordpress-submissions/"><em>submit it here</em></a><em>.</em></p>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:30:"com-wordpress:feed-additions:1";a:1:{s:7:"post-id";a:1:{i:0;a:5:{s:4:"data";s:4:"8316";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:1;a:6:{s:4:"data";s:60:"
		
		
		
		
		
				
		
		

					
										
					
		
		
			";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:4:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:36:"People of WordPress: Robert Cheleuka";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:71:"https://wordpress.org/news/2020/01/people-of-wordpress-robert-cheleuka/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Sat, 25 Jan 2020 15:26:54 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:2:{i:0;a:5:{s:4:"data";s:9:"Community";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:9:"heropress";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://wordpress.org/news/?p=8300";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:385:"You’ve probably heard that WordPress is open-source software, and may know that it’s created and run by volunteers. WordPress enthusiasts share many examples of how WordPress changed people’s lives for the better. This monthly series shares some of those lesser-known, amazing stories. Meet Robert Cheleuka Robert is a self-taught graphic and motion designer turned web [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:16:"Yvette Sonneveld";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:8428:"
<p><em>You’ve probably heard that WordPress is open-source software, and may know that it’s created and run by volunteers. WordPress enthusiasts share many examples of how WordPress changed people’s lives for the better. This monthly series shares some of those lesser-known, amazing stories.</em></p>



<h2><strong>Meet Robert Cheleuka</strong></h2>



<p>Robert is a self-taught graphic and motion designer turned web designer (and aspiring web developer) from Malawi, Africa. Over the years, he has grown fond of WordPress and has become a loyal user. Still, the journey is rough.</p>



<figure class="wp-block-image size-large"><img src="https://i2.wp.com/wordpress.org/news/files/2020/01/DSC08818.jpg?resize=632%2C790&#038;ssl=1" alt="Robert Cheleuka" class="wp-image-8303" data-recalc-dims="1" /><figcaption>Robert Cheleuka</figcaption></figure>



<h3><strong>Malawi</strong></h3>



<p>Malawi is one of the poorest countries in the world. A tiny landlocked country with a population of 17 million, it’s largely rural and still considered a developing country. The average entry-level monthly pay for most skilled jobs is about $110. If you&#8217;re employed full-time in the creative industry and if you’re very lucky, you might be able to earn more than that. Employees earning more than $300 a month are rare to non-existent.</p>



<p>Robert has been a freelance graphic designer since about 2011. He started by doing gigs from his dorm in college and from home. Earnings from his freelance jobs increased his interest in entrepreneurship and he started to consider starting his own creative agency.</p>



<h3><strong>How Robert was introduced to WordPress</strong></h3>



<p>Robert first came into contact with WordPress in 2014 when he and a friend started a local tech blog. Before that, all he knew was basic, outdated HTML from high school and some knowledge of Adobe Dreamweaver. They decided to use WordPress, and their new blog looked like it came from the future. They used a theme from the repo and got such positive feedback from the blog they decided to open a content and media publishing agency.</p>



<p>While they got a few web redesign jobs thanks to the exposure the blog brought, they lacked the administrative and business skills needed and ended up going their separate ways. Then in his first real job after college Robert finally took it upon himself to learn the ins and outs of WordPress. He learned how to install WordPress on a server and did some research on customizing themes. </p>



<p>With that knowledge alone he got his first web design clients and started earning nearly as much as he did at his job. Robert soon realized that free WordPress themes would only take him so far, especially with his limited code skills.</p>



<p>Because in Malawi only people who travel abroad have access to credit cards, paying for premium themes was impossible. Like many WordPress designers in developing countries, Robert turned to using pirated themes instead. He knew that was both unsafe and unethical, and decided to learn how to code. Knowing how to build themes from scratch would surely help him rise above the competition. </p>



<figure class="wp-block-image size-large"><img src="https://i1.wp.com/wordpress.org/news/files/2020/01/20180905_1971270886435744_8835917058488139776_n.jpg?fit=632%2C632&amp;ssl=1" alt="" class="wp-image-8304" srcset="https://i1.wp.com/wordpress.org/news/files/2020/01/20180905_1971270886435744_8835917058488139776_n.jpg?w=1080&amp;ssl=1 1080w, https://i1.wp.com/wordpress.org/news/files/2020/01/20180905_1971270886435744_8835917058488139776_n.jpg?resize=300%2C300&amp;ssl=1 300w, https://i1.wp.com/wordpress.org/news/files/2020/01/20180905_1971270886435744_8835917058488139776_n.jpg?resize=1024%2C1024&amp;ssl=1 1024w, https://i1.wp.com/wordpress.org/news/files/2020/01/20180905_1971270886435744_8835917058488139776_n.jpg?resize=150%2C150&amp;ssl=1 150w, https://i1.wp.com/wordpress.org/news/files/2020/01/20180905_1971270886435744_8835917058488139776_n.jpg?resize=768%2C768&amp;ssl=1 768w" sizes="(max-width: 632px) 100vw, 632px" /></figure>



<h3><strong>The WordPress community from Robert’s perspective</strong></h3>



<p>Robert doesn’t have a lot of interaction with the WordPress community. Although he would search for solutions from blogs about WordPress he had never actually talked to or asked anyone from the community for a solution.&nbsp;</p>



<p>Robert believes that this isolation is the result of a glass ceiling &#8212; the WordPress community is partially online and partially in-person, but there isn’t a local group in Malawi. And because Malawi, like many other developing nations, lacks a way to pay online many can’t access premium support, online learning, or most other types of professional development. No matter how welcoming the people of WordPress might be, it can still feel like it mostly belongs to those with enough privilege to conduct business on the internet.</p>



<h3><strong>WordPress &amp; inclusion</strong></h3>



<p>As most freelancers know, it’s really hard to learn while you also still need to earn. Add pitching to clients and shipping graphic design projects… there are only so many hours in a day.</p>



<p>Robert didn’t have a programming background and had always been more of a creative person. In order to grow as a web designer/developer, he needed to learn PHP. Again, without access to a credit card, that was complicated. Also, free coding training wasn’t as widely available as it is now.</p>



<p>Robert wishes that more developers would consider alternative ways for users who cannot pay for courses, themes, or plugins (whether that’s because of available infrastructure or otherwise). He wishes that WordPress tutors and developers would open up ways to accommodate aspiring learners in developing countries who cannot access plugins, courses, and themes, to be able to give back and to participate at another level.</p>



<p>WordPress has allowed him to build an income he would have no other way of earning and it makes a huge difference. He believes sharing stories like his will hopefully make WordPress products and services become more universally available. In addition, he hopes that more aspiring, self-taught developers will find courage in reaching out to connect with others out there.</p>



<h2><strong>Contributors</strong></h2>



<p>Alison Rothwell (<a href='https://profiles.wordpress.org/wpfiddlybits/' class='mention'><span class='mentions-prefix'>@</span>wpfiddlybits</a>), Yvette Sonneveld (<a href='https://profiles.wordpress.org/yvettesonneveld/' class='mention'><span class='mentions-prefix'>@</span>yvettesonneveld</a>), Josepha Haden (<a href='https://profiles.wordpress.org/chanthaboune/' class='mention'><span class='mentions-prefix'>@</span>chanthaboune</a>), Siobhan Cunningham (<a href='https://profiles.wordpress.org/siobhanseija/' class='mention'><span class='mentions-prefix'>@</span>siobhanseija</a>), Topher DeRosia (<a href='https://profiles.wordpress.org/topher1kenobe/' class='mention'><span class='mentions-prefix'>@</span>topher1kenobe</a>)</p>



<div class="wp-block-columns">
<div class="wp-block-column" style="flex-basis:33.33%">
<figure class="wp-block-image size-large"><img src="https://i1.wp.com/wordpress.org/news/files/2019/07/heropress_large_white_logo.jpg?resize=632%2C474&#038;ssl=1" alt="" class="wp-image-7025" srcset="https://i1.wp.com/wordpress.org/news/files/2019/07/heropress_large_white_logo.jpg?w=1024&amp;ssl=1 1024w, https://i1.wp.com/wordpress.org/news/files/2019/07/heropress_large_white_logo.jpg?resize=300%2C225&amp;ssl=1 300w, https://i1.wp.com/wordpress.org/news/files/2019/07/heropress_large_white_logo.jpg?resize=768%2C576&amp;ssl=1 768w" sizes="(max-width: 632px) 100vw, 632px" data-recalc-dims="1" /></figure>
</div>



<div class="wp-block-column" style="flex-basis:66.66%">
<p> <em>This post is based on an article originally published on HeroPress.com, a community initiative created by <a href="https://profiles.wordpress.org/topher1kenobe/">Topher DeRosia</a>. HeroPress highlights people in the WordPress community who have overcome barriers and whose stories would otherwise go unheard.</em> </p>
</div>
</div>



<p> <em>Meet more WordPress community members over at </em><a href="https://heropress.com/"><em>HeroPress.com</em></a><em>!</em> </p>



<p><br></p>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:30:"com-wordpress:feed-additions:1";a:1:{s:7:"post-id";a:1:{i:0;a:5:{s:4:"data";s:4:"8300";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:2;a:6:{s:4:"data";s:60:"
		
		
		
		
		
				
		
		

					
										
					
		
		
			";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:4:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:42:"WordPress Leaders Nominated for CMX Awards";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:78:"https://wordpress.org/news/2020/01/wordpress-leaders-nominated-for-cmx-awards/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 15 Jan 2020 21:42:12 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:2:{i:0;a:5:{s:4:"data";s:6:"Awards";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:9:"Community";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://wordpress.org/news/?p=8288";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:366:"Two members of the WordPress leadership team were nominated for excellent work in their field in the first ever Community Industry Awards. Andrea Middleton is nominated for Executive Leader of a Community Team and Josepha Haden Chomphosy is nominated for Community Professional of the Year. CMX is one of the largest professional organizations dedicated to [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:16:"Francesca Marano";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:4014:"
<p>Two members of the WordPress leadership team were nominated for excellent work in their field in the first ever Community Industry Awards. <a href="https://profiles.wordpress.org/andreamiddleton/">Andrea Middleton</a> is nominated for <a href="https://cmxhub.com/awards-executive">Executive Leader of a Community Team</a> and <a href="https://profiles.wordpress.org/chanthaboune/">Josepha Haden Chomphosy</a> is nominated for <a href="https://cmxhub.com/awards-professional">Community Professional of the Year</a>.</p>



<figure class="wp-block-image size-large"><img src="https://i0.wp.com/wordpress.org/news/files/2020/01/josepha_and_andrea.jpg?fit=632%2C281&amp;ssl=1" alt="" class="wp-image-8292" srcset="https://i0.wp.com/wordpress.org/news/files/2020/01/josepha_and_andrea.jpg?w=2412&amp;ssl=1 2412w, https://i0.wp.com/wordpress.org/news/files/2020/01/josepha_and_andrea.jpg?resize=300%2C133&amp;ssl=1 300w, https://i0.wp.com/wordpress.org/news/files/2020/01/josepha_and_andrea.jpg?resize=1024%2C455&amp;ssl=1 1024w, https://i0.wp.com/wordpress.org/news/files/2020/01/josepha_and_andrea.jpg?resize=768%2C341&amp;ssl=1 768w, https://i0.wp.com/wordpress.org/news/files/2020/01/josepha_and_andrea.jpg?resize=1536%2C683&amp;ssl=1 1536w, https://i0.wp.com/wordpress.org/news/files/2020/01/josepha_and_andrea.jpg?resize=2048%2C910&amp;ssl=1 2048w, https://i0.wp.com/wordpress.org/news/files/2020/01/josepha_and_andrea.jpg?w=1264&amp;ssl=1 1264w, https://i0.wp.com/wordpress.org/news/files/2020/01/josepha_and_andrea.jpg?w=1896&amp;ssl=1 1896w" sizes="(max-width: 632px) 100vw, 632px" /></figure>



<p><a href="https://cmxhub.com/">CMX</a> is one of the largest professional organizations dedicated to community builders. The awards were open to public nomination, and finalists were chosen by panels of their peers in the CMX community.</p>



<p>Andrea has been a vital community strategist for the WordPress project since 2011. Her work to build and support a vibrant community has played a part in the success around the popular open source CMS. Her work is sponsored by Automattic, where she leads a team that focuses on educational efforts, funding, and in-person community-driven events that serve a global base.</p>



<p>Josepha has been the Executive Director of the WordPress project since 2019. Her work to coordinate and guide volunteer efforts spans 20 teams and involves thousands of volunteers. Her work is also sponsored by Automattic, where she leads the open source division that focuses on all aspects of open source contribution including design, development, volunteer engagement, and the health of the overall WordPress ecosystem.</p>



<h2>Votes are Open</h2>



<p>Final recipients are chosen with open voting — if you feel like either Andrea or Josepha have had an impact on your careers, your trajectory in the WordPress project, or the health of WordPress as a whole, there are three ways you can show your support:</p>



<ul><li>Stop by and vote for them (Andrea&nbsp;<a href="https://href.li/?https://cmxhub.com/awards-executive">here</a>, Josepha&nbsp;<a href="https://href.li/?https://cmxhub.com/awards-professional">here</a>)!</li><li>Share this post with your own communities!</li><li>Tweet some inspirational thoughts about your time/experience/learnings with WordPress (using&nbsp;#WordPress, naturally)!</li></ul>



<h2>Thank You Notes</h2>



<blockquote class="wp-block-quote"><p>A lot of care and passion goes into making the WordPress Project as fantastic as it is. I think these awards are a reflection of how wonderful the community and ecosystem are, and I appreciate everyone&#8217;s continued trust in my stewardship!</p><cite>Josepha Haden Chomphosy</cite></blockquote>



<blockquote class="wp-block-quote"><p>WordPress community organizers are some of the most generous and creative people in the world — working with them is exciting and interesting every day. I’m humbled by this nomination; thank you!</p><cite>Andrea Middleton</cite></blockquote>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:30:"com-wordpress:feed-additions:1";a:1:{s:7:"post-id";a:1:{i:0;a:5:{s:4:"data";s:4:"8288";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:3;a:6:{s:4:"data";s:57:"
		
		
		
		
		
				
		

					
										
					
		
		
			";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:4:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:37:"The Month in WordPress: December 2019";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:72:"https://wordpress.org/news/2020/01/the-month-in-wordpress-december-2019/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 03 Jan 2020 17:05:16 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:18:"Month in WordPress";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://wordpress.org/news/?p=8282";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:353:"As 2019 draws to a close and we look ahead to another exciting year let’s take a moment to review what the WordPress community achieved in December. WordPress 5.3.1 and 5.3.2 Releases The WordPress 5.3.1 security and maintenance release was announced on December 13. It features 46 fixes and enhancements. This version corrects four security [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:10:"Angela Jin";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:8186:"
<p>As 2019 draws to a close and we look ahead to another exciting year let’s take a moment to review what the WordPress community achieved in December.</p>



<hr class="wp-block-separator" />



<h2>WordPress 5.3.1 and 5.3.2 Releases</h2>



<p>The WordPress 5.3.1 security and maintenance release was announced on December 13. It features 46 fixes and enhancements. This version corrects four security issues in WordPress versions 5.3 and earlier. Shortly afterwards, WordPress 5.3.2 was released, addressing a couple high severity Trac tickets, and includes 5 fixes and enhancements, so you’ll want to upgrade. You can read more about these releases in the announcements for <a href="https://wordpress.org/news/2019/12/wordpress-5-3-1-security-and-maintenance-release/">5.3.1</a> and <a href="https://wordpress.org/news/2019/12/wordpress-5-3-2-maintenance-release/">5.3.2</a>.</p>



<h2>Update on the Nine Core Projects for 2019</h2>



<p>At the end of 2018, <a href='https://profiles.wordpress.org/matt/' class='mention'><span class='mentions-prefix'>@</span>matt</a> <a href="https://make.wordpress.org/core/2018/12/08/9-priorities-for-2019/">announced</a> the nine projects that would be the main focus areas for Core development in the next year. Have we made progress? Yes! <a href='https://profiles.wordpress.org/chanthaboune/' class='mention'><span class='mentions-prefix'>@</span>chanthaboune</a> <a href="https://make.wordpress.org/core/2019/12/06/update-9-projects-for-2019/">posted a full update</a> on the team’s work. In brief, two of the projects have been completed and shipped in major releases, four are targeted for release in versions 5.4 and 5.5 of WordPress, and the remaining three have seen significant progress but are not yet slated for completion. These will continue to see progress throughout 2020.</p>



<p>Want to get involved in building WordPress Core? Follow <a href="https://make.wordpress.org/core/">the Core team blog</a> and join the #core channel in <a href="https://make.wordpress.org/chat/">the Making WordPress Slack group</a>.</p>



<h2>WordPress Major Release Calendar</h2>



<p>The Core team has published <a href="https://make.wordpress.org/core/2019/11/21/tentative-release-calendar-2020-2021/">a tentative release calendar</a> for 2020 and 2021. This is intended to provide the community with more information about what lies ahead.</p>



<p>The schedule is considered tentative because there are always variables that could affect these plans — not least that the Core team may need more time to finish the work planned for a release.</p>



<h2>Initial Documentation for Block-Based WordPress Themes</h2>



<p>The Gutenberg team has started working on <a href="https://github.com/WordPress/gutenberg/pull/18890/files">the initial documentation</a> for what block-based themes might look like, marking a significant change in the way themes are conceptualized. With full-site editing now a realistic goal for WordPress, themes will certainly look different in the future.</p>



<p>Want to help shape the future of block-based themes in WordPress Core? Following <a href="https://make.wordpress.org/core/">the Core team blog</a> is a good start! You can also join in on the discussion <a href="https://make.wordpress.org/themes/2019/12/04/questions-about-the-future-of-themes/">on this blog post</a>, or help out with the work to <a href="https://github.com/WordPress/gutenberg/blob/master/lib/demo-block-templates/index.html">create a demo space for experimentation with the future of themes</a>. As always, contribution to <a href="https://github.com/WordPress/gutenberg/">Gutenberg on GitHub</a> is open to everyone! Join the #core-editor channel in <a href="https://make.wordpress.org/chat/">the Making WordPress Slack group</a> to see what other people are saying, and contribute your own thoughts.</p>



<h2>Gutenberg Updates Abound</h2>



<p>It’s been a busy month for Gutenberg! Version 7.0, including a new navigation block, <a href="https://make.wordpress.org/core/2019/11/27/whats-new-in-gutenberg-27-november/">was announced</a> on November 27. This was followed by <a href="https://make.wordpress.org/core/2019/12/11/whats-new-in-gutenberg-11-december/">version 7.1</a>, announced on December 11; it includes 161 merged pull requests that offer a fresh UI to new users, an option to switch between edit and navigation modes, captions for the table block, and many other enhancements.</p>



<p>Want to get involved in building Gutenberg? Follow <a href="https://make.wordpress.org/core/">the Core team blog</a>, contribute to <a href="https://github.com/WordPress/gutenberg/">Gutenberg on GitHub</a>, and join the #core-editor channel in <a href="https://make.wordpress.org/chat/">the Making WordPress Slack group</a>.</p>



<h2>Arrival of the BuddyPress Beta Tester Plugin</h2>



<p>On December 2, <a href="https://buddypress.org/2019/12/bp-beta-tester-1-0-0/">the BuddyPress Beta Tester plugin</a> was added to the WordPress.org plugins directory. This feature is a great way for the WordPress community to provide early feedback on releases.</p>



<p>You can <a href="https://wordpress.org/plugins/bp-beta-tester/">download the plugin</a> now. If you find that something is not working as expected during your beta tests, let the BuddyPress team know by submitting a ticket on the <a href="https://buddypress.trac.wordpress.org/newticket">Development Tracker</a> or posting a new topic in the BuddyPress <a href="https://buddypress.org/support/">support forums</a>.​​</p>



<h2>An Update on the Block Directory in the WordPress Editor&nbsp;</h2>



<p>The Design team received lots of excellent feedback on the <a href="https://make.wordpress.org/design/2019/07/11/block-directory-in-wp-admin-concepts/">early concepts for the Block Directory</a>. This feedback was incorporated into a Version 1 update to the <a href="https://make.wordpress.org/design/tag/block-directory/">#block-directory</a> project. The Block Directory is to be included in WordPress 5.5, which is slated for August 2020. To learn more about the Block Directory, check out <a href="https://make.wordpress.org/design/2019/12/19/block-directory-in-wp-admin-v1/">this announcement post</a> and help out by sharing your feedback. </p>



<p>Want to get involved in building the Block Directory? Follow <a href="https://make.wordpress.org/design/">the Design team blog</a>. If you have a block you’d like to include in the directory you can <a href="https://make.wordpress.org/meta/2019/12/06/block-directory-plugin-guidelines/">submit it following the information here</a>. </p>



<hr class="wp-block-separator" />



<h2>Further Reading:</h2>



<ul><li><a href="https://make.wordpress.org/meta/2019/12/06/block-directory-plugin-guidelines/">Guidelines for the Block Directory</a> have been drafted; the team is actively working on them now.</li><li>The<a href="https://make.wordpress.org/community/2020-wordpress-global-community-sponsorship-program/"> Global Community Sponsorship Program for 2020</a> has been announced.&nbsp;</li><li>The Theme Review Team <a href="https://make.wordpress.org/themes/2019/12/09/do-not-contact-reviewers-outside-of-the-wordpress-org-system-about-your-review/">has published a reminder</a> for developers about the proper way to communicate with reviewers.</li><li>The Community Team is in the process of <a href="https://make.wordpress.org/community/2019/12/05/community-team-reps-for-2020/">selecting new team reps</a>.</li><li><a href="https://meetup.com/pro/wordpress">The WordPress meetup program</a> crossed the 800-group mark this month and includes groups from more than 100 countries.</li><li>The team that helped to create the 2019 State of the Word slide deck <a href="https://wordpress.org/news/2019/12/state-of-the-word-the-story-of-the-slides/">shared how the slides were created using Gutenberg</a>, powered by the Slides plugin.&nbsp;</li></ul>



<p><em>Have a story that we should include in the next “Month in WordPress” post? Please </em><a href="https://make.wordpress.org/community/month-in-wordpress-submissions/"><em>submit it here</em></a><em>.</em></p>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:30:"com-wordpress:feed-additions:1";a:1:{s:7:"post-id";a:1:{i:0;a:5:{s:4:"data";s:4:"8282";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:4;a:6:{s:4:"data";s:57:"
		
		
		
		
		
				
		

					
										
					
		
		
			";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:4:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:35:"WordPress 5.3.2 Maintenance Release";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:71:"https://wordpress.org/news/2019/12/wordpress-5-3-2-maintenance-release/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 18 Dec 2019 22:42:26 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:8:"Releases";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://wordpress.org/news/?p=8275";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:391:"WordPress 5.3.2 is now available! This maintenance release features 5 fixes and enhancements. WordPress 5.3.2 is a short-cycle maintenance release. The next major release will be&#160;version 5.4. You can download WordPress 5.3.2 by clicking the button at the top of this page, or visit your&#160;Dashboard → Updates&#160;and click&#160;Update Now. If you have sites that support [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:9:"Jb Audras";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:2874:"
<p>WordPress 5.3.2 is now available!</p>



<p>This maintenance release features 5 fixes and enhancements.</p>



<p>WordPress 5.3.2 is a short-cycle maintenance release. The next major release will be&nbsp;version 5.4.</p>



<p>You can download WordPress 5.3.2 by clicking the button at the top of this page, or visit your<strong>&nbsp;Dashboard → Updates</strong>&nbsp;and click&nbsp;<strong>Update Now</strong>.</p>



<p>If you have sites that support automatic background updates, they’ve already started the update process.</p>



<h2>Maintenance updates</h2>



<p>Shortly after&nbsp;<a href="https://wordpress.org/news/2019/12/wordpress-5-3-1-security-and-maintenance-release/">WordPress 5.3.1 was released</a>, a couple of high severity Trac tickets were opened. The Core team scheduled this quick maintenance release to resolve these issues.</p>



<p>Main issues addressed in 5.3.2:</p>



<ul><li>Date/Time: Ensure that&nbsp;<code>get_feed_build_date()</code>&nbsp;correctly handles a modified post object with invalid date.</li><li>Uploads: Fix file name collision in&nbsp;<code>wp_unique_filename()</code>&nbsp;when uploading a file with upper case extension on non case-sensitive file systems.</li><li>Media: Fix PHP warnings in&nbsp;<code>wp_unique_filename()</code>&nbsp;when the destination directory is unreadable.</li><li>Administration: Fix the colors in all color schemes for buttons with the&nbsp;<code>.active</code>&nbsp;class.</li><li>Posts, Post Types: In&nbsp;<code>wp_insert_post()</code>, when checking the post date to set&nbsp;<code>future</code>&nbsp;or&nbsp;<code>publish</code>&nbsp;status, use a proper delta comparison.</li></ul>



<p>For more information,&nbsp;<a href="https://core.trac.wordpress.org/query?status=closed&amp;type=!task+(blessed)&amp;resolution=fixed&amp;milestone=5.3.2&amp;col=id&amp;col=summary&amp;col=owner&amp;col=type&amp;col=priority&amp;col=component&amp;col=version&amp;order=priority">browse the full list of changes on Trac</a>&nbsp;or check out the&nbsp;<a href="https://wordpress.org/support/wordpress-version/version-5-3-2/">version 5.3.2 HelpHub documentation page</a>.</p>



<h2>Thanks!</h2>



<p>Thank you to everyone who contributed to WordPress 5.3.2:</p>



<p><a href="https://profiles.wordpress.org/azaozz/">Andrew Ozz</a>, <a href="https://profiles.wordpress.org/rarst/">Andrey &#8220;Rarst&#8221; Savchenko</a>, <a href="https://profiles.wordpress.org/dd32/">Dion hulse</a>, <a href="https://profiles.wordpress.org/eden159/">eden159</a>, <a href="https://profiles.wordpress.org/audrasjb/">Jb Audras</a>, <a href="https://profiles.wordpress.org/ryelle/">Kelly Dwan</a>, <a href="https://profiles.wordpress.org/pbiron/">Paul Biron</a>, <a href="https://profiles.wordpress.org/sergeybiryukov/">Sergey Biryukov</a>, <a href="https://profiles.wordpress.org/tellyworth/">Tellyworth</a>.</p>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:30:"com-wordpress:feed-additions:1";a:1:{s:7:"post-id";a:1:{i:0;a:5:{s:4:"data";s:4:"8275";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:5;a:6:{s:4:"data";s:57:"
		
		
		
		
		
				
		

					
										
					
		
		
			";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:4:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:42:"State of the Word: the story of the slides";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:77:"https://wordpress.org/news/2019/12/state-of-the-word-the-story-of-the-slides/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 17 Dec 2019 19:27:01 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:7:"General";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://wordpress.org/news/?p=8233";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:142:"Curious about the Gutenberg powered slides used during State of the Word? This post uncovers some technical and design aspects of the project!";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"Ella van Durpe";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:11767:"
<p>During the State of the Word at WordCamp US 2019, Matt Mullenweg shared that Gutenberg was used to create his slides and the presentation was powered by the Slides plugin. Using WordPress to power a slide deck isn’t an obvious choice, so we wanted to showcase the process and give some tips for making slide layouts using Gutenberg.</p>



<p><em>This post is co-written by Ella and <a href="https://profiles.wordpress.org/karmatosed/">Tammie</a>, who (along with <a href="https://profiles.wordpress.org/melchoyce/">Mel</a>, <a href="https://profiles.wordpress.org/mapk/">Mark</a>, <a href="https://profiles.wordpress.org/nrqsnchz/">Enrique</a>, <a href="https://profiles.wordpress.org/itsjonq/">Q</a></em> <em>and a cast of supporters) helped create this year’s State of the Word slide deck.</em></p>



<h2>How it Started</h2>



<p><a href="https://profiles.wordpress.org/ellatrix/">Ella Van Durpe</a> was selected to speak at <a href="https://www.youtube.com/watch?v=ZNWNhUPrqB4">JSConf</a> and ReactEurope and wanted slides for her presentation.&nbsp;</p>



<p>In the past, she’d used Reveal.js to create slides and enjoyed the freedom to create anything using HTML, CSS and JavaScript. These languages were comfortable, familiar, and also can be published on the web in their native format.&nbsp;</p>



<p>For these new presentations, she wanted to use Reveal.js again but didn’t feel like writing all the HTML by hand. Creating blocks of content visually, without having to actually write any code, which can be published natively to the web, is exactly what Gutenberg was built for.</p>



<p>The plugin was prototyped quickly, with hardcoded styles on the slides and zero options. At the end of each presentation, Ella shared a brief demo of the Gutenberg-based slides and the audience was amazed.</p>



<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter"><div class="wp-block-embed__wrapper">
<blockquote class="twitter-tweet" data-width="550" data-dnt="true"><p lang="en" dir="ltr">.<a href="https://twitter.com/ellatrx?ref_src=twsrc%5Etfw">@ellatrx</a> any chance you might open source this? <img src="https://s.w.org/images/core/emoji/12.0.0-1/72x72/1f642.png" alt="🙂" class="wp-smiley" style="height: 1em; max-height: 1em;" /> <a href="https://t.co/6hygMpBqUA">https://t.co/6hygMpBqUA</a></p>&mdash; Pascal Birchler (@swissspidy) <a href="https://twitter.com/swissspidy/status/1131573525612048385?ref_src=twsrc%5Etfw">May 23, 2019</a></blockquote><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</div></figure>



<p>As WordCamp US approached, Ella suggested that her plugin be used for State of the Word. Since it was such a hit with her audience members, it seemed like this would be a great chance to share it with the WordPress community as a whole.</p>



<span id="more-8233"></span>



<h2>How it Works</h2>



<h3>Technical information</h3>



<p>The plugin registers a custom post type called “presentation” and a new “slide” block. The slide block is a sort of enhanced “Group” block, which is restricted to the root of the post, but you can put any other block inside it. As a result, you have a post filled with only slides at the root and slides filled with content. This maps perfectly to the Reveal.js markup, which requires content in HTML section elements.</p>



<p>Since these slides each have their own design, the theme styles are disabled and styling is set from each slide. A custom presentation template is provided by the plugin to render the custom post on the front-end.</p>



<p>Then Ella added options to style the slides. For example, she added options to control the slide background and font, at both the document level and individual slide level. She also added Reveal.js options, which allow you to change the transition style and speed. Lastly, she added a custom CSS field to allow style overwrites.</p>



<p>For the State of the Word, she also added speaker notes and a speaker view.</p>



<figure class="wp-block-image size-large"><img src="https://i0.wp.com/wordpress.org/news/files/2019/12/image.png?fit=632%2C363&amp;ssl=1" alt="" class="wp-image-8271" srcset="https://i0.wp.com/wordpress.org/news/files/2019/12/image.png?w=1256&amp;ssl=1 1256w, https://i0.wp.com/wordpress.org/news/files/2019/12/image.png?resize=300%2C172&amp;ssl=1 300w, https://i0.wp.com/wordpress.org/news/files/2019/12/image.png?resize=1024%2C588&amp;ssl=1 1024w, https://i0.wp.com/wordpress.org/news/files/2019/12/image.png?resize=768%2C441&amp;ssl=1 768w" sizes="(max-width: 632px) 100vw, 632px" /></figure>



<h3>How it&#8217;s Designed</h3>



<figure class="wp-block-image size-large"><img src="https://i0.wp.com/wordpress.org/news/files/2019/12/01.png?fit=632%2C515&amp;ssl=1" alt="" class="wp-image-8262" srcset="https://i0.wp.com/wordpress.org/news/files/2019/12/01.png?w=1114&amp;ssl=1 1114w, https://i0.wp.com/wordpress.org/news/files/2019/12/01.png?resize=300%2C245&amp;ssl=1 300w, https://i0.wp.com/wordpress.org/news/files/2019/12/01.png?resize=1024%2C835&amp;ssl=1 1024w, https://i0.wp.com/wordpress.org/news/files/2019/12/01.png?resize=768%2C626&amp;ssl=1 768w" sizes="(max-width: 632px) 100vw, 632px" /></figure>



<p>The inspiration for the slide designs came from Blue Note album covers (which were also the inspiration for the <a href="https://www.slideshare.net/photomatt/state-of-the-word-2011/">2011 State of the Word slides</a>). These album covers inspired the recent “About” page in WordPress core and the starter content for the new Twenty Twenty theme. This style consists of strong, geometric shapes and simple forms with clean lines. Photography and bold typography are at the heart of this look.</p>



<figure class="wp-block-image size-large"><img src="https://i1.wp.com/wordpress.org/news/files/2019/12/image-17.png?fit=632%2C541&amp;ssl=1" alt="" class="wp-image-8267" srcset="https://i1.wp.com/wordpress.org/news/files/2019/12/image-17.png?w=3586&amp;ssl=1 3586w, https://i1.wp.com/wordpress.org/news/files/2019/12/image-17.png?resize=300%2C257&amp;ssl=1 300w, https://i1.wp.com/wordpress.org/news/files/2019/12/image-17.png?resize=1024%2C877&amp;ssl=1 1024w, https://i1.wp.com/wordpress.org/news/files/2019/12/image-17.png?resize=768%2C658&amp;ssl=1 768w, https://i1.wp.com/wordpress.org/news/files/2019/12/image-17.png?resize=1536%2C1316&amp;ssl=1 1536w, https://i1.wp.com/wordpress.org/news/files/2019/12/image-17.png?resize=2048%2C1754&amp;ssl=1 2048w, https://i1.wp.com/wordpress.org/news/files/2019/12/image-17.png?w=1264&amp;ssl=1 1264w, https://i1.wp.com/wordpress.org/news/files/2019/12/image-17.png?w=1896&amp;ssl=1 1896w" sizes="(max-width: 632px) 100vw, 632px" /></figure>



<p>Various color palettes and font pairings were explored before selecting the best combination for the presentation.</p>



<figure class="wp-block-image size-large"><img src="https://i0.wp.com/wordpress.org/news/files/2019/12/03.png?fit=632%2C407&amp;ssl=1" alt="" class="wp-image-8259" srcset="https://i0.wp.com/wordpress.org/news/files/2019/12/03.png?w=1311&amp;ssl=1 1311w, https://i0.wp.com/wordpress.org/news/files/2019/12/03.png?resize=300%2C193&amp;ssl=1 300w, https://i0.wp.com/wordpress.org/news/files/2019/12/03.png?resize=1024%2C660&amp;ssl=1 1024w, https://i0.wp.com/wordpress.org/news/files/2019/12/03.png?resize=768%2C495&amp;ssl=1 768w" sizes="(max-width: 632px) 100vw, 632px" /></figure>



<p>A strong photographic element was needed for the slides. Past WordCamps are a great source of photos, so we dug through years of photo galleries to find just the right shots. With colors, fonts, and photos, we laid the foundation to build the deck.</p>



<h2>Tips for Making Gutenberg Slides</h2>



<h3>Tip One: columns are your friends</h3>



<p>If you want slides with a precise layout, use columns. As you can see here, we used a 3 column layout to center content within a slide:</p>



<figure class="wp-block-image size-large"><img src="https://i0.wp.com/wordpress.org/news/files/2019/12/3.png?w=632&#038;ssl=1" alt="" class="wp-image-8260" srcset="https://i0.wp.com/wordpress.org/news/files/2019/12/3.png?w=874&amp;ssl=1 874w, https://i0.wp.com/wordpress.org/news/files/2019/12/3.png?resize=300%2C168&amp;ssl=1 300w, https://i0.wp.com/wordpress.org/news/files/2019/12/3.png?resize=768%2C431&amp;ssl=1 768w" sizes="(max-width: 632px) 100vw, 632px" data-recalc-dims="1" /></figure>



<h3>Tip Two: spacer blocks are useful</h3>



<p>Want to really unlock the power of columns? Combine them with the spacer block! We used spacer blocks to position content over background images, like in this slide:</p>



<figure class="wp-block-image size-large"><img src="https://i2.wp.com/wordpress.org/news/files/2019/12/6.png?w=632&#038;ssl=1" alt="" class="wp-image-8264" srcset="https://i2.wp.com/wordpress.org/news/files/2019/12/6.png?w=865&amp;ssl=1 865w, https://i2.wp.com/wordpress.org/news/files/2019/12/6.png?resize=300%2C171&amp;ssl=1 300w, https://i2.wp.com/wordpress.org/news/files/2019/12/6.png?resize=768%2C438&amp;ssl=1 768w" sizes="(max-width: 632px) 100vw, 632px" data-recalc-dims="1" /></figure>



<h3>Tip Three: test on large screens</h3>



<p>It’s important to preview and test your slides as you go. Make sure to design for the size and aspect ratio of the projector you’ll be using and do a visual check in presentation mode from time to time.</p>



<figure class="wp-block-image size-large"><img src="https://i0.wp.com/wordpress.org/news/files/2019/12/notes.png?fit=632%2C405&amp;ssl=1" alt="" class="wp-image-8269" srcset="https://i0.wp.com/wordpress.org/news/files/2019/12/notes.png?w=1096&amp;ssl=1 1096w, https://i0.wp.com/wordpress.org/news/files/2019/12/notes.png?resize=300%2C192&amp;ssl=1 300w, https://i0.wp.com/wordpress.org/news/files/2019/12/notes.png?resize=1024%2C657&amp;ssl=1 1024w, https://i0.wp.com/wordpress.org/news/files/2019/12/notes.png?resize=768%2C493&amp;ssl=1 768w" sizes="(max-width: 632px) 100vw, 632px" /></figure>



<h3>Tip Four: check your videos</h3>



<p>A good demo video is integral to showcase new features. Joen Asmussen has a great post on <a href="https://automattic.design/2019/11/12/good-ui-demo-videos/">creating effective demo videos</a>.</p>



<h2>Lessons learned</h2>



<blockquote class="wp-block-quote"><p><em>I’d love to make the art directed compositions easier to create. </em></p><cite><em>Mel Choyce-Dwan</em></cite></blockquote>



<p>As Gutenberg evolves, one big improvement is that art direction will get easier. Even with the improvements this year, creating some layouts in Gutenberg was trickier than expected. For the more complicated compositions, we relied on SVGs. Eventually, the need for hacks will dissolve away, and a new world of exciting possibilities will open for everyone.</p>



<p>Browsers offered one of our biggest learnings in this presentation, more than they would if you use Keynote or Powerpoint, for example (tools most of us have used). Often, we found that what we created in the editor varied a when viewed full-screen. We were able to mitigate this by updating the plugin to use a fixed size, instead of using the entire browser window.</p>



<h2>Wrapping it up</h2>



<p>If you would like to check out the State of the Word, you can watch the <a href="https://wordpress.tv/2019/11/03/2019-state-of-the-word/">video</a> and read all about it in a <a href="https://ma.tt/2019/11/state-of-the-word-2019/">post</a>.</p>



<p>The Slides plugin is not only available on the plugin repo, but you can also get the code from <a href="https://github.com/WordPress/slides">GitHub</a> and <a href="https://translate.wordpress.org/projects/wp-plugins/slide/">help translate</a>.</p>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:30:"com-wordpress:feed-additions:1";a:1:{s:7:"post-id";a:1:{i:0;a:5:{s:4:"data";s:4:"8233";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:6;a:6:{s:4:"data";s:60:"
		
		
		
		
		
				
		
		

					
										
					
		
		
			";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:4:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:48:"WordPress 5.3.1 Security and Maintenance Release";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:84:"https://wordpress.org/news/2019/12/wordpress-5-3-1-security-and-maintenance-release/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 13 Dec 2019 00:07:06 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:2:{i:0;a:5:{s:4:"data";s:8:"Releases";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:8:"Security";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://wordpress.org/news/?p=8203";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:360:"WordPress 5.3.1 is now available! This security and maintenance release features 46 fixes and enhancements. Plus, it adds a number of security fixes—see the list below. WordPress 5.3.1 is a short-cycle maintenance release. The next major release will be version 5.4. You can download WordPress 5.3.1 by clicking the button at the top of this page, [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:9:"Jb Audras";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:12709:"
<p>WordPress 5.3.1 is now available!</p>



<p>This security and maintenance release features 46 fixes and enhancements. Plus, it adds a number of security fixes—see the list below.</p>



<p>WordPress 5.3.1 is a short-cycle maintenance release. The next major release will be version 5.4.</p>



<p>You can download WordPress 5.3.1 by clicking the button at the top of this page, or visit your<strong>&nbsp;Dashboard → Updates</strong>&nbsp;and click&nbsp;<strong>Update Now</strong>.</p>



<p>If you have sites that support automatic background updates, they’ve already started the update process.</p>



<h2>Security updates</h2>



<p>Four security issues affect WordPress versions 5.3 and earlier; version 5.3.1 fixes them, so you’ll want to upgrade. If you haven’t yet updated to 5.3, there are also updated versions of 5.2 and earlier that fix the security issues.</p>



<ul><li>Props to <a href="https://danielbachhuber.com/">Daniel Bachhuber</a> for finding an issue where an unprivileged user could make a post sticky via the REST API.</li><li>Props to <a href="https://blog.ripstech.com/authors/simon-scannell">Simon Scannell of RIPS Technologies</a> for finding and disclosing an issue where cross-site scripting (XSS) could be stored in well-crafted links.</li><li>Props to the <a rel="noreferrer noopener" target="_blank" href="https://wordpress.org/">WordPress.org</a> Security Team for hardening <code>wp_kses_bad_protocol()</code> to ensure that it is aware of the named colon attribute.</li><li>Props to <a href="https://twitter.com/ducnt_">Nguyen The Duc</a> for discovering a stored XSS vulnerability using block editor content.</li></ul>



<h2>Maintenance updates</h2>



<p>Here are a few of the highlights:</p>



<ul><li>Administration: improvements to admin form controls height and alignment standardization (see related <a href="https://make.wordpress.org/core/2019/12/11/admin-form-controls-height-and-alignment-standardization-in-wordpress-5-3-1/">dev note</a>), dashboard widget links accessibility and alternate color scheme readability issues (see related <a href="https://make.wordpress.org/core/2019/12/10/alternate-color-schemes-changes-in-wordpress-5-3-1/">dev note</a>).</li><li>Block editor: fix Edge scrolling issues and intermittent JavaScript issues.</li><li>Bundled themes: add customizer option to show/hide author bio, replace JS based smooth scroll with CSS (see related <a href="https://make.wordpress.org/core/2019/12/08/twenty-twenty-animated-scroll-changes-in-wordpress-5-3-1/">dev note</a>) and fix Instagram embed CSS.</li><li>Date/time: improve non-GMT dates calculation, fix date format output in specific languages and make&nbsp;<code>get_permalink()</code>&nbsp;more resilient against PHP timezone changes.</li><li>Embeds: remove CollegeHumor oEmbed provider as the service doesn&#8217;t exist anymore.</li><li>External libraries: update <code>sodium_compat</code>.</li><li>Site health: allow the remind interval for the admin email verification to be filtered.</li><li>Uploads: avoid thumbnails overwriting other uploads when filename matches, and exclude PNG images from scaling after upload.</li><li>Users: ensure administration email verification uses the user&#8217;s locale instead of the site locale.</li></ul>



<p>For more information, <a href="https://core.trac.wordpress.org/query?status=closed&amp;resolution=fixed&amp;milestone=5.3.1&amp;order=priority">browse the full list of changes on Trac</a> or check out the&nbsp;<a href="https://wordpress.org/support/wordpress-version/version-5-3-1/">version 5.3.1 HelpHub documentation page</a>.</p>



<h2>Thanks!</h2>



<p>In addition to the security researchers mentioned above, thank you to everyone who contributed to WordPress 5.3.1:</p>



<p><a href="https://profiles.wordpress.org/123host/">123host</a>, <a href="https://profiles.wordpress.org/acosmin/">acosmin</a>, <a href="https://profiles.wordpress.org/adamsilverstein/">Adam Silverstein</a>, <a href="https://profiles.wordpress.org/aljullu/">Albert Juhé Lluveras</a>, <a href="https://profiles.wordpress.org/xknown/">Alex Concha</a>, <a href="https://profiles.wordpress.org/viper007bond/">Alex Mills</a>, <a href="https://profiles.wordpress.org/anantajitjg/">Anantajit JG</a>, <a href="https://profiles.wordpress.org/anlino/">Anders Norén</a>, <a href="https://profiles.wordpress.org/andraganescu/">andraganescu</a>, <a href="https://profiles.wordpress.org/afercia/">Andrea Fercia</a>, <a href="https://profiles.wordpress.org/aduth/">Andrew Duthie</a>, <a href="https://profiles.wordpress.org/azaozz/">Andrew Ozz</a>, <a href="https://profiles.wordpress.org/rarst/">Andrey &#8220;Rarst&#8221; Savchenko</a>, <a href="https://profiles.wordpress.org/aravindajith/">aravindajith</a>, <a href="https://profiles.wordpress.org/archon810/">archon810</a>, <a href="https://profiles.wordpress.org/ate-up-with-motor/">Ate Up With Motor</a>, <a href="https://profiles.wordpress.org/ayeshrajans/">Ayesh Karunaratne</a>, <a href="https://profiles.wordpress.org/birgire/">Birgir Erlendsson (birgire)</a>, <a href="https://profiles.wordpress.org/boga86/">Boga86</a>, <a href="https://profiles.wordpress.org/boonebgorges/">Boone Gorges</a>, <a href="https://profiles.wordpress.org/poena/">Carolina Nymark</a>, <a href="https://profiles.wordpress.org/chetan200891/">Chetan Prajapati</a>, <a href="https://profiles.wordpress.org/littlebigthing/">Csaba (LittleBigThings)</a>, <a href="https://profiles.wordpress.org/xendo/">Dademaru</a>, <a href="https://profiles.wordpress.org/danielbachhuber/">Daniel Bachhuber</a>, <a href="https://profiles.wordpress.org/mte90/">Daniele Scasciafratte</a>, <a href="https://profiles.wordpress.org/talldanwp/">Daniel Richards</a>, <a href="https://profiles.wordpress.org/davidbaumwald/">David Baumwald</a>, <a href="https://profiles.wordpress.org/dlh/">David Herrera</a>, <a href="https://profiles.wordpress.org/dd32/">Dion hulse</a>, <a href="https://profiles.wordpress.org/ehtis/">ehtis</a>, <a href="https://profiles.wordpress.org/ellatrix/">Ella van Durpe</a>, <a href="https://profiles.wordpress.org/epiqueras/">epiqueras</a>, <a href="https://profiles.wordpress.org/fabifott/">Fabian</a>, <a href="https://profiles.wordpress.org/flixos90/">Felix Arntz</a>, <a href="https://profiles.wordpress.org/flaviozavan/">flaviozavan</a>, <a href="https://profiles.wordpress.org/garrett-eclipse/">Garrett Hyder</a>, <a href="https://profiles.wordpress.org/hometowntrailers/">Glenn</a>, <a href="https://profiles.wordpress.org/gziolo/">Grzegorz (Greg) Ziółkowski</a>, <a href="https://profiles.wordpress.org/grzegorzjanoszka/">Grzegorz.Janoszka</a>, <a href="https://profiles.wordpress.org/hareesh-pillai/">Hareesh Pillai</a>, <a href="https://profiles.wordpress.org/ianbelanger/">Ian Belanger</a>, <a href="https://profiles.wordpress.org/ispreview/">ispreview</a>, <a href="https://profiles.wordpress.org/whyisjake/">Jake Spurlock</a>, <a href="https://profiles.wordpress.org/macmanx/">James Huff</a>, <a href="https://profiles.wordpress.org/jameskoster/">James Koster</a>, <a href="https://profiles.wordpress.org/jarretc/">Jarret</a>, <a href="https://profiles.wordpress.org/studiotwee/">Jasper van der Meer</a>, <a href="https://profiles.wordpress.org/audrasjb/">Jb Audras</a>, <a href="https://profiles.wordpress.org/jeichorn/">jeichorn</a>, <a href="https://profiles.wordpress.org/jeremyclarke/">Jer Clarke</a>, <a href="https://profiles.wordpress.org/jeremyfelt/">Jeremy Felt</a>, <a href="https://profiles.wordpress.org/jipmoors/">Jip Moors</a>, <a href="https://profiles.wordpress.org/joehoyle/">Joe Hoyle</a>, <a href="https://profiles.wordpress.org/johnjamesjacoby/">John James Jacoby</a>, <a href="https://profiles.wordpress.org/desrosj/">Jonathan Desrosiers</a>, <a href="https://profiles.wordpress.org/spacedmonkey/">Jonny Harris</a>, <a href="https://profiles.wordpress.org/joostdevalk/">Joost de Valk</a>, <a href="https://profiles.wordpress.org/jorgefilipecosta/">Jorge Costa</a>, <a href="https://profiles.wordpress.org/joyously/">Joy</a>, <a href="https://profiles.wordpress.org/jrf/">Juliette Reinders Folmer</a>, <a href="https://profiles.wordpress.org/justdaiv/">justdaiv</a>, <a href="https://profiles.wordpress.org/ryelle/">Kelly Dwan</a>, <a href="https://profiles.wordpress.org/kharisblank/">Kharis Sulistiyono</a>, <a href="https://profiles.wordpress.org/ixkaito/">Kite</a>, <a href="https://profiles.wordpress.org/kyliesabra/">kyliesabra</a>, <a href="https://profiles.wordpress.org/lisota/">lisota</a>, <a href="https://profiles.wordpress.org/lukaswaudentio/">lukaswaudentio</a>, <a href="https://profiles.wordpress.org/maciejmackowiak/">Maciej Mackowiak</a>, <a href="https://profiles.wordpress.org/marcelo2605/">marcelo2605</a>, <a href="https://profiles.wordpress.org/clorith/">Marius L. J.</a>, <a href="https://profiles.wordpress.org/mat-lipe/">Mat Lipe</a>, <a href="https://profiles.wordpress.org/mayanksonawat/">mayanksonawat</a>, <a href="https://profiles.wordpress.org/melchoyce/">Mel Choyce-Dwan</a>, <a href="https://profiles.wordpress.org/michael-arestad/">Michael Arestad</a>, <a href="https://profiles.wordpress.org/miette49/">miette49</a>, <a href="https://profiles.wordpress.org/mcsf/">Miguel Fonseca</a>, <a href="https://profiles.wordpress.org/mihdan/">mihdan</a>, <a href="https://profiles.wordpress.org/mauteri/">Mike Auteri</a>, <a href="https://profiles.wordpress.org/msaari/">Mikko Saari</a>, <a href="https://profiles.wordpress.org/gdragon/">Milan Petrovic</a>, <a href="https://profiles.wordpress.org/mukesh27/">Mukesh Panchal</a>, <a href="https://profiles.wordpress.org/nextscripts/">NextScripts</a>, <a href="https://profiles.wordpress.org/nickdaugherty/">Nick Daugherty</a>, <a href="https://profiles.wordpress.org/nielslange/">Niels Lange</a>, <a href="https://profiles.wordpress.org/noyle/">noyle</a>, <a href="https://profiles.wordpress.org/ov3rfly/">Ov3rfly</a>, <a href="https://profiles.wordpress.org/paragoninitiativeenterprises/">Paragon Initiative Enterprises</a>, <a href="https://profiles.wordpress.org/pbiron/">Paul Biron</a>, <a href="https://profiles.wordpress.org/peterwilsoncc/">Peter Wilson</a>, <a href="https://profiles.wordpress.org/larrach/">Rachel Peter</a>, <a href="https://profiles.wordpress.org/youknowriad/">Riad Benguella</a>, <a href="https://profiles.wordpress.org/quicoto/">Ricard Torres</a>, <a href="https://profiles.wordpress.org/murgroland/">Roland Murg</a>, <a href="https://profiles.wordpress.org/rmccue/">Ryan McCue</a>, <a href="https://profiles.wordpress.org/welcher/">Ryan Welcher</a>, <a href="https://profiles.wordpress.org/samuelfernandez/">SamuelFernandez</a>, <a href="https://profiles.wordpress.org/sathyapulse/">sathyapulse</a>, <a href="https://profiles.wordpress.org/wonderboymusic/">Scott Taylor</a>, <a href="https://profiles.wordpress.org/scvleon/">scvleon</a>, <a href="https://profiles.wordpress.org/sergeybiryukov/">Sergey Biryukov</a>, <a href="https://profiles.wordpress.org/sergiomdgomes/">sergiomdgomes</a>, <a href="https://profiles.wordpress.org/sgr33n/">SGr33n</a>, <a href="https://profiles.wordpress.org/simonjanin/">simonjanin</a>, <a href="https://profiles.wordpress.org/smerriman/">smerriman</a>, <a href="https://profiles.wordpress.org/steevithak/">steevithak</a>, <a href="https://profiles.wordpress.org/sabernhardt/">Stephen Bernhardt</a>, <a href="https://profiles.wordpress.org/netweb/">Stephen Edgar</a>, <a href="https://profiles.wordpress.org/dufresnesteven/">Steve Dufresne</a>, <a href="https://profiles.wordpress.org/subratamal/">Subrata Mal</a>, <a href="https://profiles.wordpress.org/manikmist09/">Sultan Nasir Uddin</a>, <a href="https://profiles.wordpress.org/cybr/">Sybre Waaijer</a>, <a href="https://profiles.wordpress.org/karmatosed/">Tammie Lister</a>, <a href="https://profiles.wordpress.org/tanvirul/">Tanvirul Haque</a>, <a href="https://profiles.wordpress.org/tellyworth/">Tellyworth</a>, <a href="https://profiles.wordpress.org/timon33/">timon33</a>, <a href="https://profiles.wordpress.org/timothyblynjacobs/">Timothy Jacobs</a>, <a href="https://profiles.wordpress.org/spaceshipone/">Timothée Brosille</a>, <a href="https://profiles.wordpress.org/tmatsuur/">tmatsuur</a>, <a href="https://profiles.wordpress.org/dinhtungdu/">Tung Du</a>, <a href="https://profiles.wordpress.org/veminom/">Veminom</a>, <a href="https://profiles.wordpress.org/vortfu/">vortfu</a>, <a href="https://profiles.wordpress.org/waleedt93/">waleedt93</a>, <a href="https://profiles.wordpress.org/williampatton/">williampatton</a>, <a href="https://profiles.wordpress.org/wpgurudev/">wpgurudev</a>, and <a href="https://profiles.wordpress.org/tollmanz/">Zack Tollman</a>.</p>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:30:"com-wordpress:feed-additions:1";a:1:{s:7:"post-id";a:1:{i:0;a:5:{s:4:"data";s:4:"8203";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:7;a:6:{s:4:"data";s:63:"
		
		
		
		
		
				
		
		
		

					
										
					
		
		
			";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:4:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:32:"People of WordPress: Jill Binder";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:67:"https://wordpress.org/news/2019/12/people-of-wordpress-jill-binder/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 06 Dec 2019 23:27:23 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:3:{i:0;a:5:{s:4:"data";s:9:"Community";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:9:"heropress";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:2;a:5:{s:4:"data";s:10:"Interviews";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://wordpress.org/news/?p=8192";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:377:"You’ve probably heard that WordPress is open-source software, and may know that it’s created and run by volunteers. WordPress enthusiasts share many examples of how WordPress changed people’s lives for the better. This monthly series shares some of those lesser-known, amazing stories. Meet Jill Binder Jill Binder never meant to become an activist. She insists [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:7:"Josepha";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:12123:"
<p><em>You’ve probably heard that WordPress is open-source software, and may know that it’s created and run by volunteers. WordPress enthusiasts share many examples of how WordPress changed people’s lives for the better. This monthly series shares some of those lesser-known, amazing stories.</em></p>



<h2><strong>Meet Jill Binder</strong></h2>



<p>Jill Binder never meant to become an activist. She insists it was an accident.</p>



<p>Despite that, Jill has led the Diversity Outreach Speaker Training working group in the WordPress Community team since 2017. This group is dedicated to increasing the number of women and other underrepresented groups who are stepping up to become speakers at WordPress Meetups, WordCamps, and events.&nbsp;</p>



<h2><strong>Jill’s back story</strong></h2>



<h3><strong>Internship</strong></h3>



<p>Jill’s WordPress story begins in 2011, in Vancouver, Canada.&nbsp;Jill secured an internship for her college program, working on a higher education website that was built in WordPress.&nbsp;As a thank you, her practicum advisor bought Jill a ticket to WordCamp Vancouver 2011: Developer’s Edition. After that Jill began freelancing&nbsp; with WordPress as a Solopreneur.&nbsp;</p>



<h3><strong>First steps in the WordPress community</strong></h3>



<p>The following year her internship advisor, who had become a client, was creating the first ever BuddyCamp for BuddyPress. He asked Jill to be on his organizing team. At that event she also moderated a panel that had Matt Mullenweg on it. Then, Jill was invited to be on the core organizing team for WordCamp Vancouver.</p>



<p>Part of this role meant reviewing and selecting speakers. From 40 speaker applications that could be a fit the team had to pick only 14 to speak.</p>



<h2><strong>The diversity challenge when selecting speakers</strong></h2>



<p>For anyone who has organized a conference, you know that speaker selection is hard. Of the 40 applications, 7 were from women, and the lead organizer selected 6 of those to be included in the speaker line up.</p>



<p>At this point Jill wasn’t aware that very few women apply to speak at tech conferences and suggested selection should be made on the best fit for the conference. The team shared that not only did they feel the pitches were good and fit the conference, but they also needed to be accepted or the Organizers would be criticized for a lack of diversity.</p>



<p>Selecting women for fear of criticism is embarrassing to admit, but that’s how people felt in 2013.</p>



<p>By the time the event happened, though, the number of women speakers dropped to 4. And with an additional track being added, the number of speakers overall was up to 28. Only 1 speaker in 7 was a woman (or 14%) and attendees did ask questions and even blogged about the lack of representation.</p>



<h2><strong>What keeps women from applying?</strong></h2>



<p>Later that year at&nbsp; WordCamp San Francisco—the biggest WordCamp at the time (before there was a WordCamp US)—Jill took the opportunity to chat with other organizers about her experience. She found out that many organizers had trouble getting enough women to present.</p>



<p>Surprisingly Vancouver had a high number of women applicants in comparison to others, and the consensus was more would be accepted&nbsp; if only more would apply.</p>



<p>Jill decided that she&nbsp; needed to know why this was happening? Why weren’t there more women applying? She started researching, reading, and talking to people.</p>



<p>Though this issue is complex, two things came up over and over:</p>



<ul><li>“What would I talk about?”</li><li>“I’m not an expert on anything. I don’t know enough about anything to give a talk on it.”</li></ul>



<h2><strong>A first workshop with encouraging results</strong></h2>



<p>Then Jill had an idea. She brought up the issue at an event and someone suggested that they should get women together in a room and brainstorm speaker topics.</p>



<p>So Jill became the lead of a small group creating a workshop in Vancouver: the talented Vanessa Chu, Kate Moore Hermes, and Mandi Wise. In one of the exercises that they created, participants were invited to brainstorm ideas—this proved that they had literally a hundred topic ideas and the biggest problem then became picking just one!</p>



<p>In the first workshop, they focussed on:</p>



<ul><li>Why it matters that women (<em>added later: diverse groups</em>) are in the front of the room</li><li>The myths of what it takes to be the speaker at the front of the room (aka beating impostor syndrome)</li><li>Different presentation formats, especially story-telling</li><li>Finding and refining a topic</li><li>Tips to become a better speaker</li><li>Leveling up by speaking in front of the group throughout the afternoon</li></ul>



<figure class="wp-block-image size-large"><img src="https://i2.wp.com/wordpress.org/news/files/2019/12/image-2.png?w=632&#038;ssl=1" alt="women gathering to discussion presentation topics" class="wp-image-8195" srcset="https://i2.wp.com/wordpress.org/news/files/2019/12/image-2.png?w=600&amp;ssl=1 600w, https://i2.wp.com/wordpress.org/news/files/2019/12/image-2.png?resize=300%2C200&amp;ssl=1 300w" sizes="(max-width: 600px) 100vw, 600px" data-recalc-dims="1" /><figcaption>Vancouver Workshop 2014</figcaption></figure>



<h2><strong>Leading to workshops across North America and then the world</strong></h2>



<p>Other cities across North America heard about the workshop and started hosting them, adding their own material.</p>



<p>Many women who initially joined her workshop wanted help getting even better at public speaking. So Jill&#8217;s Vancouver team added in some material created from the other cities and a bit more of their own. Such as:</p>



<ul><li>Coming up with a great title</li><li>Writing a pitch that is more likely to get accepted</li><li>Writing a bio</li><li>Creating an outline</li></ul>



<p>At WordCamp Vancouver 2014—only one year since Jill started—there were 50% women speakers and 3 times the number of women applicants! Not only that, but this WordCamp was a Developer’s Edition, where it’s more challenging to find women developers in general, let alone those who will step up to speak.</p>



<h3><strong>More work is needed!</strong></h3>



<p>Impressive as those results were, the reason Jill is so passionate about this work is because of what happened next:</p>



<ul><li>Some of the women who attended the workshop stepped up to be leaders in the community and created new content for other women.</li><li>A handful of others became WordCamp organizers. One year Vancouver had an almost all-female organizing team – 5 out of 6!</li><li>It also influenced local businesses. One local business owner loved what one of the women speakers said so much that he hired her immediately. She was the first woman developer on the team, and soon after she became the Senior Developer.</li></ul>



<h2><strong>Diversity touches on many levels</strong></h2>



<p>Jill has seen time and again what happens when different people speak at the front of the room. More people feel welcome in the community. The speakers and the new community members bring new ideas and new passions that help to make the technology we are creating more inclusive. And together we generate new ideas that benefit everyone.</p>



<p>This workshop was so successful, with typical results of going from 10% to 40-60% women speakers at WordCamps, that the WordPress Global Community Team asked Jill to promote it and train it for women and all diverse groups around the world. In late 2017, Jill started leading the Diverse Speaker Training group (<a href="https://make.wordpress.org/community/tag/wpdiversity/">#wpdiversity</a>).</p>



<p>Dozens of community members across the world have now been trained to lead the workshop. With now dozens of workshops worldwide, for WordPress and other open source software projects as well, there is an increase in speaker diversity.&nbsp;</p>



<figure class="wp-block-image size-large"><img src="https://i0.wp.com/wordpress.org/news/files/2019/12/image-3.png?fit=632%2C474&amp;ssl=1" alt="Diverse Speaker Training group " class="wp-image-8196" srcset="https://i0.wp.com/wordpress.org/news/files/2019/12/image-3.png?w=2048&amp;ssl=1 2048w, https://i0.wp.com/wordpress.org/news/files/2019/12/image-3.png?resize=300%2C225&amp;ssl=1 300w, https://i0.wp.com/wordpress.org/news/files/2019/12/image-3.png?resize=1024%2C768&amp;ssl=1 1024w, https://i0.wp.com/wordpress.org/news/files/2019/12/image-3.png?resize=768%2C576&amp;ssl=1 768w, https://i0.wp.com/wordpress.org/news/files/2019/12/image-3.png?resize=1536%2C1152&amp;ssl=1 1536w, https://i0.wp.com/wordpress.org/news/files/2019/12/image-3.png?w=1264&amp;ssl=1 1264w, https://i0.wp.com/wordpress.org/news/files/2019/12/image-3.png?w=1896&amp;ssl=1 1896w" sizes="(max-width: 632px) 100vw, 632px" /><figcaption>WordCamp US 2019</figcaption></figure>



<p>As a result of the success, Jill is now sponsored to continue the program. The first sponsor is Automattic. She’s proud of how the diversity represented on the stage adds value not only to the brand but also in the long-term will lead to the creation of a better product. She’s inspired by seeing the communities change as a result of the new voices and new ideas at the WordPress events.</p>



<blockquote class="wp-block-quote"><p><em>Jill’s leadership in the development and growth of the Diversity Outreach Speaker Training initiative has had a positive, measurable impact on WordPress community events worldwide. When WordPress events are more diverse, the WordPress project gets more diverse — which makes WordPress better for more people.”</em></p><cite><em> Andrea Middleton, Community organizer on the WordPress open source project</em></cite></blockquote>



<h3><strong>Resources:</strong></h3>



<ul><li>The Workshop: <a href="http://diversespeakers.info/">http://diversespeakers.info/</a></li><li>More information: <a href="https://tiny.cc/wpdiversity">https://tiny.cc/wpdiversity</a></li><li><a href="https://make.wordpress.org/community/handbook/wordcamp-organizer/planning-details/speakers/building-a-diverse-speaker-roster/">How to build a diverse speaker roster</a></li><li><a href="https://make.wordpress.org/community/2017/11/13/call-for-volunteers-diversity-outreach-speaker-training/">Diversity Outreach Speaker Training Team</a></li></ul>



<h2><strong>Contributors</strong></h2>



<p>Alison Rothwell (<a href="https://profiles.wordpress.org/wpfiddlybits/">@wpfiddlybits</a>), Yvette Sonneveld (<a href="https://profiles.wordpress.org/yvettesonneveld/">@yvettesonneveld</a>), Josepha Haden (<a href="https://profiles.wordpress.org/chanthaboune/">@chanthaboune</a>), Topher DeRosia (<a href="https://profiles.wordpress.org/topher1kenobe/">@topher1kenobe</a>)</p>



<div class="wp-block-image"><figure class="alignleft is-resized"><img src="https://lh6.googleusercontent.com/fq6qus5qmviDZaznrQnW-4wcbSs6NSrqeqEEGnPjgi2WJrVevNm4Em4KsP-VVH_0kMgWuNtW7mm_V9-hKtrrJFohRi6KrUXAoLHjrymChCltMr9fuh4dBIu_0SqNPts0MZgcvh_W" alt="" width="153" height="115" /></figure></div>



<p><em>This post is based on an article originally published on HeroPress.com, a community initiative created by </em><a href="https://profiles.wordpress.org/topher1kenobe/"><em>Topher DeRosia</em></a><em>. HeroPress highlights people in the WordPress community who have overcome barriers and whose stories would otherwise go unheard.</em></p>



<p></p>



<p><em>Meet more WordPress community members over at </em><a href="https://heropress.com/"><em>HeroPress.com</em></a><em>!</em></p>



<p><em><strong>Correction: December 7, 2019</strong><br>The original article mentioned the team Jill lead, but did not mention the team members who joined her. Those have been added. Apologies to Vanessa, Kate, and Mandi. <img src="https://s.w.org/images/core/emoji/12.0.0-1/72x72/1f642.png" alt="🙂" class="wp-smiley" style="height: 1em; max-height: 1em;" /> </em></p>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:30:"com-wordpress:feed-additions:1";a:1:{s:7:"post-id";a:1:{i:0;a:5:{s:4:"data";s:4:"8192";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:8;a:6:{s:4:"data";s:57:"
		
		
		
		
		
				
		

					
										
					
		
		
			";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:4:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:37:"The Month in WordPress: November 2019";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:72:"https://wordpress.org/news/2019/12/the-month-in-wordpress-november-2019/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 02 Dec 2019 08:38:00 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:18:"Month in WordPress";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://wordpress.org/news/?p=8156";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:330:"November has been a big month in the WordPress community. New releases, big events, and a push for more contributors have characterized the work being done across the project — read on to find out more! The release of WordPress 5.3 “Kirk” WordPress 5.3 was released on November 12, and is available for download or [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:15:"Hugh Lashbrooke";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:8119:"
<p>November has been a big month in the WordPress community. New releases, big events, and a push for more contributors have characterized the work being done across the project — read on to find out more!</p>



<hr class="wp-block-separator" />



<h2>The release of WordPress 5.3 “Kirk”</h2>



<p><a href="https://wordpress.org/news/2019/11/kirk/">WordPress 5.3 was released</a> on November 12, and is <a href="https://wordpress.org/download/">available for download</a> or update in your dashboard! Named “Kirk,” after jazz multi-instrumentalist Rahsaan Roland Kirk, 5.3 enhances the block editor with <a href="https://make.wordpress.org/core/2019/09/24/new-block-apis-in-wordpress-5-3/">new APIs</a> and <a href="https://make.wordpress.org/core/2019/09/27/block-editor-theme-related-updates-in-wordpress-5-3/">theme-related features</a>, adds more intuitive interactions, and improves accessibility in a number of areas — including <a href="https://make.wordpress.org/core/2019/10/18/noteworthy-admin-css-changes-in-wordpress-5-3/">CSS in the dashboard</a>, the <a href="https://make.wordpress.org/core/2019/10/14/improvements-in-media-component-accessibility-in-wordpress-5-3/">media manager</a>, <a href="https://make.wordpress.org/core/2019/09/23/core-widgets-new-aria-current-attribute-in-wordpress-5-3/">core widgets</a>, and <a href="https://core.trac.wordpress.org/query?focuses=~accessibility&amp;milestone=5.3&amp;group=component&amp;max=500&amp;col=id&amp;col=summary&amp;col=milestone&amp;col=owner&amp;col=type&amp;col=status&amp;col=priority&amp;order=id">dozens of other areas</a>.</p>



<p>You can read the full details of all the included enhancements in the <a href="https://make.wordpress.org/core/2019/10/17/wordpress-5-3-field-guide/">5.3 Field Guide</a>.</p>



<p>Along with 5.3 came <a href="https://wordpress.org/themes/twentytwenty/">the new Twenty Twenty theme</a>, which gives users more design flexibility and integrates with the block editor. For more information about the improvements to the block editor, expanded design flexibility, the Twenty Twenty theme, and to see the huge list of amazing contributors who made this release possible, read <a href="https://wordpress.org/news/2019/11/kirk/">the full announcement</a>.</p>



<p>Want to get involved in building WordPress Core? Follow <a href="https://make.wordpress.org/core/">the Core team blog</a> and join the #core channel in <a href="https://make.wordpress.org/chat/">the Making WordPress Slack group</a>. You can also <a href="https://make.wordpress.org/core/2019/11/15/5-3-retrospective-call-for-feedback/">provide feedback</a> on the 5.3 release process.</p>



<h2>At Last! bbPress 2.6!</h2>



<p><a href="https://bbpress.org/blog/2019/11/bbpress-2-6/">bbPress 2.6 was released</a> on November 12 after a little over six years in development. This new release includes per-forum moderation, new platforms to import from, and an extensible engagements API. You can read more about all of this in <a href="https://codex.bbpress.org/">the bbPress codex</a>.</p>



<p><a href="https://bbpress.org/blog/2019/11/bbpress-2-6-1-is-out/">Version 2.6.1</a> and <a href="https://bbpress.org/blog/2019/11/bbpress-2-6-2-is-out/">2.6.2</a> quickly followed, both of which fixed a number of bugs that required immediate attention.</p>



<p>Want to get involved in building bbPress? Follow <a href="https://bbpress.org/blog/">the bbPress blog</a> and join the #bbpress channel in <a href="https://make.wordpress.org/chat/">the Making WordPress Slack group</a>.</p>



<h2>State of the Word</h2>



<p><a href="https://2019.us.wordcamp.org/">WordCamp US 2019</a> was held in St. Louis, MO this year on November 1-3. At the event, <a href='https://profiles.wordpress.org/matt/' class='mention'><span class='mentions-prefix'>@</span>matt</a> gave his annual State of the Word address, during which he shared what had been accomplished in the past year, announced what is coming next, and shared several ways to get involved.</p>



<p>You can watch <a href="https://www.youtube.com/watch?v=LezbkeV059Q&amp;t=21s">the State of the Word</a> as well as <a href="https://www.youtube.com/watch?v=fFsVbAo8HwI">the Q&amp;A session at the end</a>, and <a href="https://ma.tt/2019/11/state-of-the-word-2019/">read Matt’s recap</a> of the address. If you didn’t make it to St. Louis, you can still <a href="https://www.youtube.com/playlist?list=PL1pJFUVKQ7ETHl165LvLVXfB3yBZEzV-q">watch all the sessions</a> at your leisure.</p>



<h2>Five for the Future</h2>



<p>During the State of the Word, Matt announced that there is now <a href="https://wordpress.org/five-for-the-future/">a dedicated landing page for Five for the Future</a>, which features the people and organizations that commit at least it 5% of their resources to the WordPress open source project. There are many ways to contribute to WordPress, such as core development, marketing, translation, training, and community organizing, among many other important paths to contribution.</p>



<p>Five for the Future welcomes individuals and organizations, and highlights all the incredible ways we build WordPress together. For more information, visit <a href="https://wordpress.org/five-for-the-future/">the Five for the Future page</a>.<br></p>



<hr class="wp-block-separator" />



<h2>Further Reading:</h2>



<ul><li>After releasing WordPress 5.3, the Core team announced <a href="https://make.wordpress.org/core/2019/11/21/tentative-release-calendar-2020-2021/">a tentative release schedule</a> for 2020 and 2021.</li><li>The Core team has announced <a href="https://make.wordpress.org/core/2019/11/28/new-css-focus-in-core/">a new CSS focus</a> to complement the existing ones for PHP and JavaScript — this focus comes with dedicated tags, targeted work, and a new #core-css Slack channel.</li><li>Version 2.2 of the WordPress Coding Standards <a href="https://github.com/WordPress/WordPress-Coding-Standards/releases/tag/2.2.0">has been released</a> — this new release is ready for WordPress 5.3, includes five brand new sniffs, and plenty of new command-line documentation.</li><li>The latest update to the Theme Review Coding Standards, <a href="https://github.com/WPTRT/WPThemeReview/releases/tag/0.2.1">v0.2.1</a>, is compatible with v2.2 of the WordPress Coding Standards, and helps authors to build more standards-compatible themes.</li><li><a href="https://2019.us.wordcamp.org/2019/11/11/wordcamp-us-2020/">The WordCamp US team has announced</a> the dates for next year’s event in St. Louis, MO — WordCamp US 2020 will be held on October 27-29. This will be the first time that the event will be held during the week and not on a weekend. The team has also announced a Call for Organizers. If you are interested in joining the team, <a href="https://2020.us.wordcamp.org/2019/11/21/join-the-wcus-2020-organizing-team/">learn more</a>.&nbsp;</li><li>The WP Notify project, which is building a unified notification system for WordPress Core, <a href="https://make.wordpress.org/core/2019/11/29/wp-notify-hiatus-till-january-2020/">is on hiatus</a> until January 2020.</li><li>A working group on the Community Team <a href="https://make.wordpress.org/community/2019/11/25/handbook-update-how-to-improve-the-diversity-of-your-wordpress-events/">has updated their Handbook</a> to help organizers create more diverse events.</li><li>The WP-CLI team <a href="https://make.wordpress.org/cli/2019/11/12/wp-cli-v2-4-0-release-notes/">released v2.4.0</a> of the WordPress command-line tool. This release includes support for WordPress 5.3 and PHP 7.4.</li><li>Gutenberg development continues rapidly with <a href="https://make.wordpress.org/core/2019/11/27/whats-new-in-gutenberg-27-november/">the latest 7.0 release</a> including an early version of the navigation menus block, among other enhancements and fixes.</li></ul>



<p><em>Have a story that we should include in the next “Month in WordPress” post? Please </em><a href="https://make.wordpress.org/community/month-in-wordpress-submissions/"><em>submit it here</em></a><em>.</em></p>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:30:"com-wordpress:feed-additions:1";a:1:{s:7:"post-id";a:1:{i:0;a:5:{s:4:"data";s:4:"8156";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:9;a:6:{s:4:"data";s:60:"
		
		
		
		
		
				
		
		

					
										
					
		
		
			";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:4:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:22:"WordPress 5.2.4 Update";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:58:"https://wordpress.org/news/2019/11/wordpress-5-2-4-update/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 19 Nov 2019 04:47:30 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:2:{i:0;a:5:{s:4:"data";s:8:"Releases";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:8:"Security";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://wordpress.org/news/?p=7787";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:346:"Late-breaking news on the 5.2.4 short-cycle security release that landed October 14. When we released the news post, I inadvertently missed giving props to Simon Scannell of RIPS Technologies for finding and disclosing an issue where path traversal can lead to remote code execution. Simon has done a great deal of work on the WordPress [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Jake Spurlock";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:1102:"
<p>Late-breaking news on the<a href="https://wordpress.org/news/2019/10/wordpress-5-2-4-security-release/"> 5.2.4 short-cycle security release </a>that landed October 14. When we released the news post, I inadvertently missed giving props to Simon Scannell of <a href="https://blog.ripstech.com/">RIPS Technologies</a> for finding and disclosing an issue where path traversal can lead to remote code execution. </p>



<p>Simon has done a <a href="https://wordpress.org/news/2018/12/wordpress-5-0-1-security-release/">great</a> <a href="https://wordpress.org/news/2019/03/wordpress-5-1-1-security-and-maintenance-release/">deal</a> of <a href="https://wordpress.org/news/2019/09/wordpress-5-2-3-security-and-maintenance-release/">work</a> on the WordPress project, and failing to mention his contributions is a huge oversight on our end.</p>



<p>Thank you to all of the reporters for <a href="https://make.wordpress.org/core/handbook/testing/reporting-security-vulnerabilities/">privately disclosing</a> vulnerabilities, which gave us time to fix them before WordPress sites could be attacked.</p>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:30:"com-wordpress:feed-additions:1";a:1:{s:7:"post-id";a:1:{i:0;a:5:{s:4:"data";s:4:"7787";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}s:27:"http://www.w3.org/2005/Atom";a:1:{s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:3:{s:4:"href";s:32:"https://wordpress.org/news/feed/";s:3:"rel";s:4:"self";s:4:"type";s:19:"application/rss+xml";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:44:"http://purl.org/rss/1.0/modules/syndication/";a:2:{s:12:"updatePeriod";a:1:{i:0;a:5:{s:4:"data";s:9:"
	hourly	";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:15:"updateFrequency";a:1:{i:0;a:5:{s:4:"data";s:4:"
	1	";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:30:"com-wordpress:feed-additions:1";a:1:{s:4:"site";a:1:{i:0;a:5:{s:4:"data";s:8:"14607090";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}}}}}}s:4:"type";i:128;s:7:"headers";O:42:"Requests_Utility_CaseInsensitiveDictionary":1:{s:7:" * data";a:9:{s:6:"server";s:5:"nginx";s:4:"date";s:29:"Mon, 10 Feb 2020 05:09:49 GMT";s:12:"content-type";s:34:"application/rss+xml; charset=UTF-8";s:25:"strict-transport-security";s:11:"max-age=360";s:6:"x-olaf";s:3:"⛄";s:13:"last-modified";s:29:"Mon, 03 Feb 2020 09:54:06 GMT";s:4:"link";s:63:"<https://wordpress.org/news/wp-json/>; rel="https://api.w.org/"";s:15:"x-frame-options";s:10:"SAMEORIGIN";s:4:"x-nc";s:9:"HIT ord 2";}}s:5:"build";s:14:"20191030115042";}";}